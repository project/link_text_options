<?php

/**
 * @file
 * Limits link field text to a set of options.
 */

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Implements hook_help().
 */
function link_text_options_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.link_text_options':
      $output = '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('To configure the options a site editor is able to set for a link field, edit the fields settings') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function link_text_options_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, array $form, FormStateInterface $form_state) {
  $plugin_id = $plugin->getPluginId();
  $element = [];
  if ($plugin_id == 'link_default') {
    $current_values = $plugin->getThirdPartySetting('link_text_options', 'link_text_options_allowed_values');
    $element = [
      '#type' => 'details',
      '#title' => t('Link Text Options'),
      '#open' => (bool) $current_values,
      'link_text_options_allowed_values' => [
        '#type' => 'textarea',
        '#title' => t('Allowed values list'),
        '#rows' => 5,
        '#default_value' => $current_values,
        '#description' => t('Allowed values for the link text. One per
          line. Adding values here will turn the link text textfield into a
          select dropdown. If only one option is added, it is prefilled and the
          textfield is disabled.'),
      ],
    ];
  }
  return $element;
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function link_text_options_field_widget_settings_summary_alter(array &$summary, array $context) {
  $plugin_id = $context['widget']->getPluginId();
  if ($plugin_id == 'link_default' && $context['widget']->getThirdPartySetting('link_text_options', 'link_text_options_allowed_values')) {
    $summary[] = t('link text options enabled');
  }
}

/**
 * Implements hook_field_widget_single_element_WIDGET_TYPE_form_alter().
 */
function link_text_options_field_widget_single_element_link_default_form_alter(array &$element, FormStateInterface $form_state, array $context) {
  $options = link_text_options_get_allowed_values($context['widget']);
  if (count($options)) {
    if (count($options) === 1) {
      $element['title']['#default_value'] = $options[0];
      $element['title']['#disabled'] = TRUE;
      if (!$element['#required']) {
        // Remove the state that forces the URI to required of title set.
        unset($element['uri']['#states']['required']);
        if (isset($element['#element_validate'])) {
          foreach ($element['#element_validate'] as $key => $value) {
            if (isset($value[1]) && ($value[1] == 'validateTitleNoLink')) {
              unset($element['#element_validate'][$key]);
            }
          }
        }
      }
    }
    else {
      $element['title']['#type'] = 'select';
      $element['title']['#options'] = array_combine($options, $options);
    }
  }
}

/**
 * Returns the array of allowed values set for a link widget.
 *
 * @param \Drupal\link\Plugin\Field\FieldWidget\LinkWidget $widget
 *   LinkWidget to get the allowed values for.
 *
 * @return array
 *   An array of allowed values.
 */
function link_text_options_get_allowed_values(LinkWidget $widget) {
  $options_string = $widget->getThirdPartySetting('link_text_options', 'link_text_options_allowed_values');
  if (!is_string($options_string) || strlen(trim($options_string)) === 0) {
    return [];
  }
  $list = preg_split("/\r\n|\n|\r/", $options_string);
  $list = array_map('Drupal\Component\Utility\Xss::filter', $list);
  return array_filter(array_map('trim', $list));
}
