# Link Text Options

The Link Text Options module allows you to change the link text input on a link field from a textfield into a select/options element effectively restricting what text a content editor can enter for link text.

eg. You have a node with a link field that is output as a button, but you only want the button to have certain text.
Note though, the module does not validate the value.
Also, when changing the options in the settings, previous values are not checked, so a link field could have a value that is not one of the options if it was set before the options settings were saved.

If there is only 1 option for the link text, then this option is set as default and the link text textfield is disabled.
